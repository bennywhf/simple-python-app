from flask import Flask
import uuid
app = Flask(__name__)

INSTANCE_ID = uuid.uuid4()

@app.route('/')
def main():
    return str(INSTANCE_ID)

if __name__ == '__main__':
    app.run('0.0.0.0')
