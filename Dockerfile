FROM ubuntu:16.04

RUN apt-get update -y && \
    apt-get install -y python3-pip python3-dev

COPY ./app /app
WORKDIR /app
RUN pip3 install -r requirements.txt
WORKDIR /app

CMD [ "python3", "app.py" ]
